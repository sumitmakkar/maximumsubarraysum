#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arr;
    
    public:
        Engine(vector<int> a)
        {
            arr = a;
        }
    
        int findMaximumSubArraySum()
        {
            int currentSum = arr[0];
            int finalSum   = arr[0];
            int len        = (int)arr.size();
            for(int i = 1 ; i < len ; i++)
            {
                currentSum = (currentSum + arr[i] > arr[i]) ? (currentSum + arr[i]) : arr[i];
                finalSum   = currentSum > finalSum ? currentSum : finalSum;
            }
            return finalSum;
        }
};

int main(int argc, const char * argv[])
{
//    vector<int> arr = {-2 , -5 , 6 , -2 , -3 , 1 , 5 , -6};
//    vector<int> arr = {-2 , 1 , -3 , 4 , -1 , 2 , 1 , -5 , 4};
    vector<int> arr = {2 , -9 , 5 , 1 , -4 , 6 , 0 , -7 , 8};
    Engine      e   = Engine(arr);
    cout<<e.findMaximumSubArraySum()<<endl;
    return 0;
}
